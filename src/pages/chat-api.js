import axios from 'axios';

// Replace with the actual API endpoint for a public text-gen model on Hugging Face
const baseUrl = 'https://ae.pi-inference.huggingfacco/models/gpt-2/gpt2';
// Replace with your Hugging Face access token
const accessToken = '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';

export async function loadModel() {
  // GPT-2 model loads automatically on first request, no need for manual loading.
  console.log('GPT-2 model ready for use!');
}

export async function sendMessage(message) {
  const headers = {
    Authorization: `Bearer ${accessToken}`,
  };
  const data = {
    inputs: message,
    max_length: 50, // Adjust maximum generated text length as desired
  };

  try {
    const response = await axios.post(baseUrl, data, { headers });
    return response.data.generated_text;
  } catch (error) {
    console.error('Error sending message to Hugging Face model:', error);
    throw error; // Re-throw the error for handling in the component
  }
}
