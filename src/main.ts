
  import { createApp } from 'vue'
  import { createRouter, createWebHistory } from "vue-router";
  import App from "./App.vue";
  

  import Frame from './pages/Frame.vue';
  import "./global.css";
  import Home from './pages/home.vue';
;

  interface Route {
    path: string;
    name: string;
    component: any;
  }


  const routes: Route[] = [{
          path: "/interview",
          name: "Frame",
          component: Frame
        },
        {
          path: "/",
          name: "home",
          component: Home
        },
      ]

  const router = createRouter({
    history: createWebHistory(),
    routes,
  });

  router.beforeEach((toRoute, fromRoute, next) => {
    const documentTitle = toRoute?.meta && toRoute?.meta?.title ? toRoute?.meta?.title : "wael-chafei's-team-library";
    window.document.title = documentTitle;
    if (toRoute?.meta?.description) {
      addMetaTag(toRoute?.meta?.description)
    }
    next();
  })
  
  const addMetaTag = (value) => {
    let element = document.querySelector(`meta[name='description']`);
  
    if (element) {
      element.setAttribute("content", value);
    } else {
      element = `<meta name="description" content="${value}" />`;
      document.head.insertAdjacentHTML("beforeend", element);
    }
  };

  

  createApp(App).use(router).mount('#app')
  
  export default router