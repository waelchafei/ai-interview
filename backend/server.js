// Backend
const express = require('express');
const multer = require('multer');
const fs = require('fs');
const pdfParse = require('pdf-parse');
const cors = require('cors');
const app = express();
const port = 3000;

// Set up multer for handling multipart/form-data
const upload = multer({ dest: 'uploads/' });
app.use(cors());

// Handle PDF chunk upload endpoint
app.post('/upload-pdf-chunk', upload.single('pdfChunk'), async (req, res) => {
  try {
    // Access the uploaded PDF chunk file from req.file
    const pdfChunk = req.file;
    
    // Read the chunk file from disk
    const chunkData = fs.readFileSync(pdfChunk.path);
    
    // Extract text from the chunk
    const { text } = await pdfParse(chunkData);
    console.log("texxxxxxxxxxt",text);
    // Send extracted text as response
    res.send(text);
  } catch (error) {
    console.error('Error:', error);
    res.status(500).send('Internal Server Error');
  }
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
